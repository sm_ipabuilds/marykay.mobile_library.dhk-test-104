/* global SM */

var translate = ( function( supportedLanguages , localizableStrings ) {
    "use strict";
    var currentLanguage = SM.getProperty( "#systemLanguageManager" , "applicationLanguage" );

    return {
        language: currentLanguage,
        forString: function( string ) {
            if( localizableStrings && localizableStrings[ currentLanguage ] && localizableStrings[ currentLanguage ][ string ] ) {
                return localizableStrings[ currentLanguage ][ string ];
            }
            return string;
        },
        fileWithPath: function( pathString , localLanguages ) {
            localLanguages = localLanguages || supportedLanguages;
            if( currentLanguage === "en" || localLanguages.indexOf( currentLanguage ) < 0 ) {
                return pathString;
            }

            var path = pathString.split( "/" ),
                file = path.pop().split( "." ),
                len = file.length,
                state = len >= 2 ? file[ len - 2 ].split( "-" ) : [];
            if( len === 1 || ( file[ 0 ] === "" && len === 2 ) ) {
                return pathString;
            }
            if( len >= 2 ) {
                if( state.length >= 2 ) {
                    state[ state.length - 2 ] += currentLanguage.toUpperCase();
                    file[ len - 2 ] = state.join( "-" );
                } else {
                    file[ len - 2 ] += currentLanguage.toUpperCase();
                }
            }
            path = path.length >= 1 ? path.join( "/" ) + "/" : "";
            return path + file.join( "." );
        },
        forSupportedLanguage: function( defValue , localizedValues ) {
            defValue = defValue || localizedValues.en;
            if( supportedLanguages.indexOf( currentLanguage ) && localizedValues[ currentLanguage ] ) {
                return localizedValues[ currentLanguage ];
            }
            return defValue;
        }
    };
} ( SM.getArguments().properties.supportedLanguages || [] , {
    "en": {
        "LibraryButton": "LibraryButton", // button art asset
        "OK": "OK",
        "Don't show again": "Don't show again",
        "<span style='line-height:125%;font-size:18px'><span style='font-weight: bold;'>Presentation Tools</span><br /><span style='font-size:16px;'>Swipe from the left edge of the screen to draw, share or return to the library.</span></span>": "<span style='line-height:125%;font-size:18px'><span style='font-weight: bold;'>Presentation Tools</span><br /><span style='font-size:16px;'>Swipe from the left edge of the screen to draw, share or return to the library.</span></span>"
    },
    "es": {
        "LibraryButton": "LibraryButtonES", // button art asset
        "OK": "OK",
        "Don't show again": "No mostrar de nuevo",
        "<span style='line-height:125%;font-size:18px'><span style='font-weight: bold;'>Presentation Tools</span><br /><span style='font-size:16px;'>Swipe from the left edge of the screen to draw, share or return to the library.</span></span>": "<span style='line-height:125%;font-size:18px'><span style='font-weight: bold;'>Herramientas de presentación</span><br /><span style='font-size:16px;'>Desliza el dedo desde la izquierda de la pantalla para dibujar, compartir o devolver a la biblioteca</span></span>"
    },
    "fr": {
        "LibraryButton": "LibraryButtonFR", // button art asset
        "OK": "OK",
        "Don't show again": "Ne pas montrer de nouveau",
        "<span style='line-height:125%;font-size:18px'><span style='font-weight: bold;'>Presentation Tools</span><br /><span style='font-size:16px;'>Swipe from the left edge of the screen to draw, share or return to the library.</span></span>": "<span style='line-height:125%;font-size:18px'><span style='font-weight: bold;'>Outils de présentation</span><br /><span style='font-size:16px;'>Glissez votre doigt à partir du côté gauche de l’écran pour dessiner, partager ou retourner à la bibliothèque</span></span>"
    }
} ) );