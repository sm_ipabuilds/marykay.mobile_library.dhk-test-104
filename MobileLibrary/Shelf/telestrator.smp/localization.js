/* global SM */

var translate = ( function( supportedLanguages , localizableStrings ) {
    "use strict";
    var currentLanguage = SM.getProperty( "#systemLanguageManager" , "applicationLanguage" );

    return {
        forString: function( string ) {
            if( localizableStrings && localizableStrings[ currentLanguage ] && localizableStrings[ currentLanguage ][ string ] ) {
                return localizableStrings[ currentLanguage ][ string ];
            }
            return string;
        },
        language: currentLanguage
    };
} ( SM.getArguments().properties.supportedLanguages || [] , {
    "en": {
        "drawButton": "drawButton", // Button asset
        "drawOnButton": "drawOnButton" // Button asset
    },
    "es": {
        "drawButton": "drawButtonES", // Button asset
        "drawOnButton": "drawOnButtonES" // Button asset
    },
    "fr": {
        "drawButton": "drawButtonFR", // Button asset
        "drawOnButton": "drawOnButtonFR" // Button asset
    }
} ) );