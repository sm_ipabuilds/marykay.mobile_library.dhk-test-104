// Custom Javascript
// Created by Jon May X.X.X
/* global SM, SEP */

// TODO: Download item action must be built up to check for network connectivity
// See packageSelected Function for current implementation

( function( SEP ) {
    "use strict";
    var currentPackageState = {
            collectionViewId: undefined,
            state: undefined,
            packageID: undefined
    };

    var ipadButtonTemplate = function() {
            return {
                "type": "button",
                "relative": "screen",
                "styles": [ "primaryColor" ],
                "horizontalAlign": "center",
                "verticalAlign": "center",
                "x": "50%",
                "y": "50%",
                "height": "45px",
                "width": "400px",
                "cgButtonColor": "#FFFFFF",
                "cgButtonAlpha": 1,
                "cgButtonPressedColor": "#FFFFFF",
                "cgButtonPressedAlpha": 1,
                "cgButtonShineEffect": false,
                "cgUseInteriorBorder": false,
                "cgCornerRadius": "0px",
                "cgBorderColor": "#FFFFFF",
                "cgBorderWidth": "0px",
                "font": "AvenirLTStd-Medium",
                "size": "1.8em",
                "textAlign": "left"
            };
        };

    /*
        * @desc spawn overlays
        * @return undefined - 
    */
    function deviceOffLineAction() {
        SM.runAction( {
            "action": "displayAlertAction",
            "target": "alert_plugin",
            "trigger": "now",
            "data": {
                "title": SEP.updateTranslationForString( "You are not connected to the internet." ),
                "body": SEP.updateTranslationForString( "You may open anything that has already been downloaded." ),
                "image": "art_assets/noWiFi.png",
                "buttons": [
                    {
                        "text": SEP.updateTranslationForString( "Cancel" ),
                        "actions": [
                            {
                                "action": "dismissAlertAction",
                                "trigger": "touchUpInside",
                                "target": "alert_plugin"
                            }
                        ]
                    }
                ]
            }
        } );
    }
    function closePackageModal() {
        SEP.device.callbackOnDevice( "ipad" , function() {
            SM.runAction( {
                "action": "animate",
                "trigger": "now",
                "target": "collectionView_packageModal_details",
                "data": {
                    "animationId": "anim_scaleOut"
                }
            } );

            // Remove the the secondary modal for sending email and deleting package
            SM.runAction( {
                "action": "close",
                "trigger": "now",
                "targets": [
                    "fileInfoPopOver",
                    "fileInfoPopOver_noEmail",
                    "deleteButton_container_closeBkgd"
                ]
            } );
        } );
        SEP.device.callbackOnDevice( "iphone" , function() {
            SM.runAction( {
                "action": "animate",
                "trigger": "now",
                "target": "collectionView_packageModal_details",
                "data": {
                    "animationId": "closeModal"
                }
            } );
        } );

        SM.runAction( {
            "action": "animate",
            "trigger": "now",
            "target": "collectionView_packageModal_bkgd",
            "data": {
                "animationId": "anim_fadeOut"
            }
        } );
        SM.runAction( {
            "action": "close",
            "trigger": "now",
            "delay": 0.3,
            "targets": [
                "collectionView_packageModal_details",
                "collectionView_packageModal_bkgd"
            ]
        } );
    }
    function emailPackage( emailOverlayId , fullLocalPath , packageTitle ) {
        if(arguments.length === 1 ) {
            fullLocalPath = packageSelected.localPath;
            packageTitle = packageSelected.title || "";
        }
        var emailAttachDocs = [],
            emailAttachTitles = [ packageTitle ];
        
        SM.runAction( {
            "action": "#spawnOnce",
            "trigger": "now",
            "data": {
                "overlayId": emailOverlayId,
                "deferredForAction": false,
                "subject": packageTitle,
                "text": ""
            }
        } );
        
        if( typeof fullLocalPath === "string" ) {
            emailAttachDocs[ 0 ] = fullLocalPath;
        } else if( Object.prototype.toString.call( fullLocalPath ) === "[object Array]" ) {
            emailAttachDocs = fullLocalPath;
        }

        SM.runAction( {
            "action": "attachFileAction",
            "trigger": "now",
            "target": emailOverlayId,
            "data": {
                "attachmentFilename": emailAttachDocs,
                "attachmentDisplayName": emailAttachTitles
            }
        } );
        closePackageModal();
    }

    function loadPackage( type , localPath , title, packageID ) {
        type = type.toLowerCase();
        if( type === "scrollmotion" ) {
        
            SM.runAction( {
                "action": "#loadPackage",
                "trigger": "now",
                "source": "#systemPackageDataRegistry",
                "data": {
                    "packageId": currentPackageState.packageID,
                    "transitionType": "pushin",
                    "transitionDuration": 0.4
                }
            } );
        } else if( type === "mpg" || type === "mov" || type === "mp4" ||
                   type === "mpg4" || type === "mpeg" || type === "m4v" ||
                   type === "mp3" || type === "wav" || type === "aiff" ) {
            SEP.filereaderPackage = packageID;
            loadVideoPlayer();
        } else {
            SEP.filereaderPackage = packageID;        
            loadFileReader( title );
        }
    }
    function loadFileReader( title ) {
        SM.runAction( {
            "target": "#systemPage",
            "action": "presentModalPageAction",
            "data": {
                "pageId": "page_fileReader"
            }
        } );

        SM.runAction( {
            action: "openFile",
            trigger: "now",
            delay: 0,
            target: "fileReader_filereader",
            source: currentPackageState.collectionViewId,
            data: {
                shelfItemMetaData: "#metaData"
            }
        } );

        SM.setText( "fileReader_container_title", title );
    }
    function loadVideoPlayer() {
        SM.runAction( {
            "target": "#systemPage",
            "action": "presentModalPageAction",
            "data": {
                    "pageId": "page_videoPlayer"
            }
        } );

        SM.runAction( {
            "action" : "openVideo",
            "trigger": "now",
            "source": currentPackageState.collectionViewId,
            "target": "filereader_videoPlayer",
            "data": {
                "shelfItemMetaData": "#metaData",
                "video": "#localPath"
            }
        } );
    }

    
    function downloadPackage( collectionViewId ) {
        SEP.network.callbackOnConnection( "offline" , function() {
            deviceOffLineAction();
        } );
        SEP.network.callbackOnConnection( "online" , function() {
            SM.runAction( {
                "action": "downloadItem",
                "trigger": "now",
                "source": collectionViewId,
                "target": collectionViewId,
                "data": {
                    "shelfItemMetaData": "#metaData"
                }
            } );
            closePackageModal();
        } );
    }
    function updatePackage( collectionViewId ) {
        SEP.network.callbackOnConnection( "offline" , function() {
            deviceOffLineAction();
        } );
        SEP.network.callbackOnConnection( "online" , function() {
            SM.runAction( {
                "action": "installPackage",
                "trigger": "now",
                "source": collectionViewId,
                "target": "#systemPackageDataRegistry",
                "data": {
                    "packageID": "#packageID"
                }
            } );
            closePackageModal();
        } );
    }
    function removePackage() {
        SM.runAction({
            "action": "deletePackage",
            "trigger": "now",
            "source": currentPackageState.collectionViewId,
            "target": "#systemPackageDataRegistry",
            "data": {
                "packageID": currentPackageState.packageID
            }
        });
        SM.runAction({
            "action": "refreshCellAction",
            "trigger": "now",
            "source": currentPackageState.collectionViewId,
            "target": currentPackageState.collectionViewId,
            "data": {
                "packageID": currentPackageState.packageID
            }
        });
        closePackageModal();
    }
    
    function setcurrentPackageState( collectionViewId , state , packageID ) {
        if( arguments.length === 2 ) {
            currentPackageState.collectionViewId = this.getCollectionViewId();
            currentPackageState.state = collectionViewId.toLowerCase();
            currentPackageState.packageID = state;
        } else {
            currentPackageState.collectionViewId = collectionViewId;
            currentPackageState.state = state.toLowerCase();
            currentPackageState.packageID = packageID;
            currentPackageState.installed = SM.getProperty(collectionViewId, "isInstalled");    
        }
        SEP.debug( currentPackageState );
    }
    
    currentPackageState.isInstalled = function(){
        var ret = false;
        if(currentPackageState.installed == "true"){
            ret = true;
        }
        SEP.debug("installed: " + currentPackageState.installed + " -> " + ret);
        return ret;
    }
    
    currentPackageState.isDownloading = function() {
        var state = currentPackageState.state;
        return  state == "downloadqueued"
              || state == "downloadqueuedwifi"
              || state == "downloadstarted"
              || state == "downloadpartial"
              || state == "downloadfailed"
              || state == "downloadfaileddisk"
              || state == "downloaduserpaused"
              || state == "downloadcompleted"
              || state == "downloadverificationfailed"
              || state == "backgrounddownloading";
    }
    
    function packageSelected( packageID , title , type , filetype , modifydate , filesize , localPath , version ) {
        title = replaceHTMLTags( title );

        packageSelected.packageID = packageID;
        packageSelected.title = title;
        packageSelected.type = type;
        packageSelected.filetype = filetype;
        packageSelected.modifydate = modifydate;
        packageSelected.filesize = filesize;
        packageSelected.localPath = localPath;
        packageSelected.version = version;

        var space = "&nbsp;&nbsp;|&nbsp;&nbsp;",
            infoString = filetype + space + filesize + space + modifydate,
            longPressed = ( arguments[ arguments.length - 1 ] === "longpress" ) ? true : false,
            collectionViewId = this.getCollectionViewId();

        function createHorizontalLine( y ) {
            var hLine = {
                "type" : "container",
                "relative": "parent",
                "horizontalAlign" : "center",
                "verticalAlign" : "top",
                "height" : "1px",
                "width" : "100%",
                "backgroundColor" : "#000000",
                "backgroundAlpha" : 0.10,
                "x": "50%",
                "y": y
            };
            if( y ) { hLine.y = y; }
            return hLine;
        }
        function replaceHTMLTags( string ) {
            var tags = [ "script" , "span" ],
            i = 0,
            len = tags.length,
            regex;
            for( i ; i < len ; i++ ) {
                regex = new RegExp("<" + tags[ i ] + "[^>]*>|<\/" + tags[ i ] + ">" , "g");
                string = string.replace( regex , "");
            }
            
            return string;
        }

        // Iphone button values
        var paddingX = "3px",
            marginLeft = "61px",
            marginRight = "0px",
            packageDetailOverlays = [],
            downloadButtonTemp = ipadButtonTemplate(),
            openButtonTemp = ipadButtonTemplate(),
            updateButtonTemp = ipadButtonTemplate(),
            ipadspace = "       ",

            updateAvailableButton = {
                "type": "button",
                "styles": [ "primaryColor" , "package_Update" ],
                "relative": "parent",
                "cgBorderAlpha": 0,
                "cgBorderColor": "#000000",
                "cgBorderWidth": "0px",
                "cgButtonAlpha": 0,
                "cgButtonPressedAlpha": 0,
                "cgButtonPressedColor": "#005180",
                "cgButtonShineEffect": false,
                "cgCornerRadius": "0px",
                "font": "ScrollMotion_icon",
                "fontSize": "8.4em",
                "textAlign": "center",

                // "images": [ "art_assets/iPhone_update.png" ],
                // "imagesDown": [ "art_assets/iPhone_update_tap.png" ],
                "actions": [
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.downloadPackage( '" + collectionViewId + "' );"
                        }
                    }
                ]
            },
            installUpdateButton = {
                "type": "button",
                "styles": [ "primaryColor" , "package_Update" ],
                "relative": "parent",
                "cgBorderAlpha": 0,
                "cgBorderColor": "#000000",
                "cgBorderWidth": "0px",
                "cgButtonAlpha": 0,
                "cgButtonPressedAlpha": 0,
                "cgButtonPressedColor": "#005180",
                "cgButtonShineEffect": false,
                "cgCornerRadius": "0px",
                "font": "ScrollMotion_icon",
                "fontSize": "8.4em",
                "textAlign": "center",

                // "images": [ "art_assets/iPhone_update.png" ],
                // "imagesDown": [ "art_assets/iPhone_update_tap.png" ],
                "actions": [
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.installUpdatePackage( '" + collectionViewId + "' );"
                        }
                    }
                ]
            },
            openButton = {
                "type": "button",
                "styles": [ "primaryColor" , "package_Open" ],
                "relative": "parent",
                "cgBorderAlpha": 0,
                "cgBorderColor": "#000000",
                "cgBorderWidth": "0px",
                "cgButtonAlpha": 0,
                "cgButtonPressedAlpha": 0,
                "cgButtonPressedColor": "#005180",
                "cgButtonShineEffect": false,
                "cgCornerRadius": "0px",
                "font": "ScrollMotion_icon",
                "fontSize": "8.4em",
                "textAlign": "center",

                // "images": [ "art_assets/iPhone_open.png" ],
                // "imagesDown": [ "art_assets/iPhone_open_tap.png" ],
                "actions": [
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.closePackageModal();"
                        }
                    },
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.loadPackage( '" + type + "' , '" + localPath + "' , '" + title + "' , '"+ packageID + "' );"
                        }
                    }
                ]
            },
            openExistingButton = {
                "type": "button",
                "styles": [ "primaryColor" , "package_OpenExisting" ],
                "relative": "parent",
                "cgBorderAlpha": 0,
                "cgBorderColor": "#000000",
                "cgBorderWidth": "0px",
                "cgButtonAlpha": 0,
                "cgButtonPressedAlpha": 0,
                "cgButtonPressedColor": "#005180",
                "cgButtonShineEffect": false,
                "cgCornerRadius": "0px",
                "font": "ScrollMotion_icon",
                "fontSize": "8.4em",
                "textAlign": "center",

                // "images": [ "art_assets/iPhone_open.png" ],
                // "imagesDown": [ "art_assets/iPhone_open_tap.png" ],
                "actions": [
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.closePackageModal();"
                        }
                    },
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.loadPackage( '" + type + "' , '" + localPath + "' , '" + title + "' , '"+ packageID + "' );"
                        }
                    }
                ]
            },
            removeButton = {
                "type": "button",
                "styles": [ "primaryColor" , "package_Remove" ],
                "relative": "parent",
                "cgBorderAlpha": 0,
                "cgBorderColor": "#000000",
                "cgBorderWidth": "0px",
                "cgButtonAlpha": 0,
                "cgButtonPressedAlpha": 0,
                "cgButtonPressedColor": "#005180",
                "cgButtonShineEffect": false,
                "cgCornerRadius": "0px",
                "font": "ScrollMotion_icon",
                "fontSize": "8.4em",
                "textAlign": "center",

                // "images": [ "art_assets/iPhone_remove.png" ],
                // "imagesDown": [ "art_assets/iPhone_remove_tap.png" ],
                "actions": [
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.removePackage();"
                        }
                    }
                ]
            },
            shareButton = {
                "type": "button",
                "styles": [ "primaryColor" , "package_Share" ],
                "relative": "parent",
                "cgBorderAlpha": 0,
                "cgBorderColor": "#000000",
                "cgBorderWidth": "0px",
                "cgButtonAlpha": 0,
                "cgButtonPressedAlpha": 0,
                "cgButtonPressedColor": "#005180",
                "cgButtonShineEffect": false,
                "cgCornerRadius": "0px",
                "font": "ScrollMotion_icon",
                "fontSize": "8.4em",
                "textAlign": "center",

                // "images": [ "art_assets/iPhone_share.png" ],
                // "imagesDown": [ "art_assets/iPhone_share_tap.png" ],
                "actions": [
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.emailPackage( 'global_email_iPhone','" + localPath + "', '" + title + "' , '"+ packageID + "' );"
                        }
                    }
                ]
            },
            downloadButton = {
                "type":"button",
                "styles": [ "primaryColor" , "package_Download" ],
                "relative": "parent",
                "horizontalAlign": "center",
                "verticalAlign": "top",
                "x": "50%",
                "y": "0px",
                // "width":"71px",
                // "height":"90px",

                "width": "95px",
                "height": "102px",
                "cgBorderAlpha": 0,
                "cgBorderColor": "#000000",
                "cgBorderWidth": "0px",
                "cgButtonAlpha": 0,
                "cgButtonPressedAlpha": 0,
                "cgButtonPressedColor": "#005180",
                "cgButtonShineEffect": false,
                "cgCornerRadius": "0px",
                "font": "ScrollMotion_icon",
                "fontSize": "8.4em",
                "textAlign": "center",

                // "images": [ "art_assets/iPhone_download.png" ],
                // "imagesDown": [ "art_assets/iPhone_download_tap.png" ],
                "actions": [
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.downloadPackage( '" + collectionViewId + "' );"
                        }
                    }
                ]
            };

        if( packageID === currentPackageState.packageID ) {
            
            // Launch installed packages with single tap
            if(currentPackageState.state === "installcompleted" && !longPressed ) {
                loadPackage( type , localPath , title, packageID );
            }
            else if(!longPressed && currentPackageState.isInstalled() && currentPackageState.isDownloading()){
                openButtonTemp.text = SEP.updateTranslationForString( "Open" );
                openButtonTemp.actions = [
                    {
                        "action": "dismissAlertAction",
                        "trigger": "touchUpInside",
                        "target": "alert_plugin"
                    },
                    {
                        "action": "runScriptAction",
                        "trigger": "touchUpInside",
                        "target": "#jsBridge",
                        "data": {
                            "script": "SEP.loadPackage( '" + type + "' , '" + localPath + "' , '" + title + "' , '"+ packageID + "' );"
                        }
                    }
                ]; 
                var closeButtonTemp = {};                
                closeButtonTemp.text = SEP.updateTranslationForString( "Cancel" );
                closeButtonTemp.actions = [
                    {
                        "action": "dismissAlertAction",
                        "trigger": "touchUpInside",
                        "target": "alert_plugin"
                    }
                ];                             
                SM.runAction( {
                    "action": "displayAlertAction",
                    "target": "alert_plugin",
                    "trigger": "now",
                    "data": {
                        "title": SEP.updateTranslationForString( "Download in Progress" ),
                        "body": SEP.updateTranslationForString( "You can open the existing version now, but it will automatically update once the download completes." ),
                        "buttons": [
                            openButtonTemp, closeButtonTemp
                        ]
                    }
                });
            }
            // Spawn modal if package has not been installed
            else {
                SEP.debug( "Selected Package is in the following state: " + currentPackageState.state );
                SM.spawnOnce( { overlayId: "collectionView_packageModal_bkgd" } );
                SEP.device.callbackOnDevice( "ipad" , function() {
                    SM.spawn( { "overlayId": "collectionView_packageModal_details" } );
                    SM.setText( "collectionView_packageModal_details-title", title );
                    SM.setText( "collectionView_packageModal_type", type );
                    SM.setText( "collectionView_packageModal_size", filesize );
                    SM.setText( "collectionView_packageModal_lastModify", modifydate );
                    SM.setText( "collectionView_packageModal_version", version );
                } );
                
                if( currentPackageState.state === "entitled" ) {
                    SEP.device.callbackOnDevice( "ipad" , function() {
                        downloadButtonTemp.relative = "parent";
                        downloadButtonTemp.horizontalAlign = "left";
                        downloadButtonTemp.verticalAlign = "top";
                        downloadButtonTemp.x = "0px";
                        downloadButtonTemp.y = "240px";
                        downloadButtonTemp.text = ipadspace + SEP.updateTranslationForString( "Download" );
                        downloadButtonTemp.actions = [
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.downloadPackage( '" + collectionViewId + "' );"
                                }
                            }
                        ];
                        SM.spawnInto( "collectionView_packageModal_details" , downloadButtonTemp );
                        SM.spawnInto( "collectionView_packageModal_details" , createHorizontalLine( "240px" ) );
                    } );
                    SEP.device.callbackOnDevice( "iphone" , function() {
                        packageDetailOverlays = [ downloadButton ];
                    } );
                    
                } else if(currentPackageState.isInstalled() && currentPackageState.isDownloading()){
                    SEP.device.callbackOnDevice( "ipad" , function() {                
                        openButtonTemp.relative = "parent";
                        openButtonTemp.horizontalAlign = "left";
                        openButtonTemp.verticalAlign = "top";
                        openButtonTemp.x = "0px";
                        openButtonTemp.y = "240px";
                        openButtonTemp.text = ipadspace + SEP.updateTranslationForString( "Open Existing" );
                        openButtonTemp.actions = [
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.closePackageModal();"
                                }
                            },
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.loadPackage( '" + type + "' , '" + localPath + "' , '" + title + "' , '"+ packageID + "' );"
                                }
                            }
                        ];
                        SM.spawnInto( "collectionView_packageModal_details" , openButtonTemp );
                        SM.spawnInto( "collectionView_packageModal_details" , createHorizontalLine( "240px" ) );
                    });
                    SEP.device.callbackOnDevice( "iphone" , function() {
                        // This is dependent upon the supported files title
                        if (type.toLowerCase() === "scrollmotion") {
                            marginLeft = "110px";
                            packageDetailOverlays = [ openExistingButton ];
                        }
                        else {
                            marginLeft = "110px";
                            packageDetailOverlays = [ openExistingButton ];
                        }
                    } );                    
                }
                 else if( currentPackageState.state === "installcompleted" ) {
                    SEP.device.callbackOnDevice( "ipad" , function() {
                        openButtonTemp.relative = "parent";
                        openButtonTemp.horizontalAlign = "left";
                        openButtonTemp.verticalAlign = "top";
                        openButtonTemp.x = "0px";
                        openButtonTemp.y = "240px";
                        openButtonTemp.text = ipadspace + SEP.updateTranslationForString( "Open" );
                        openButtonTemp.actions = [
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.closePackageModal();"
                                }
                            },
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.loadPackage( '" + type + "' , '" + localPath + "' , '" + title + "' , '"+ packageID + "' );"
                                }
                            }
                        ];
                        SM.spawnInto( "collectionView_packageModal_details" , openButtonTemp );
                        if( type.toLowerCase() === "scrollmotion" ) {
                            SM.spawnInto( "collectionView_packageModal_details" , { "overlayId": "action_btn_SEP_search_noEmail" } );
                        } else {
                            SM.spawnInto( "collectionView_packageModal_details" , { "overlayId": "action_btn_SEP_search" } );
                        }
                        
                        SM.spawnInto( "collectionView_packageModal_details" , createHorizontalLine( "240px" ) );
                    } );
                    SEP.device.callbackOnDevice( "iphone" , function() {
                        // This is dependent upon the supported files title
                        if (type.toLowerCase() === "scrollmotion") {
                            marginLeft = "55px";
                            packageDetailOverlays = [ openButton , removeButton ];
                        }
                        else {
                            marginLeft = "1px";
                            packageDetailOverlays = [ openButton , shareButton , removeButton ];
                        }
                    } );
                    
                } else if( currentPackageState.state === "statepolicywaituser" ) {
                    SEP.device.callbackOnDevice( "ipad" , function() {
                        openButtonTemp.relative = "parent";
                        openButtonTemp.horizontalAlign = "left";
                        openButtonTemp.verticalAlign = "top";
                        openButtonTemp.x = "0px";
                        openButtonTemp.y = "240px";
                        openButtonTemp.text = ipadspace + SEP.updateTranslationForString( "Open Existing" );
                        openButtonTemp.actions = [
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.closePackageModal();"
                                }
                            },
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.loadPackage( '" + type + "' , '" + localPath + "' , '" + title + "' , '"+ packageID + "' );"
                                }
                            }
                        ];

                        updateButtonTemp.relative = "parent";
                        updateButtonTemp.horizontalAlign = "left";
                        updateButtonTemp.verticalAlign = "top";
                        updateButtonTemp.x = "0px";
                        updateButtonTemp.y = "285px";
                        updateButtonTemp.text = ipadspace + SEP.updateTranslationForString( "Update Now" );
                        updateButtonTemp.actions = [
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.installUpdatePackage( '" + collectionViewId + "' );"
                                }
                            }
                        ];

                        SM.spawnInto( "collectionView_packageModal_details" , openButtonTemp );
                        SM.spawnInto( "collectionView_packageModal_details" , updateButtonTemp );
                        if( type.toLowerCase() === "scrollmotion" ) {
                            SM.spawnInto( "collectionView_packageModal_details" , { "overlayId": "action_btn_SEP_search_noEmail" } );
                        } else {
                            SM.spawnInto( "collectionView_packageModal_details" , { "overlayId": "action_btn_SEP_search" } );
                        }

                        SM.spawnInto( "collectionView_packageModal_details" , createHorizontalLine( "240px" ) );
                        SM.spawnInto( "collectionView_packageModal_details" , createHorizontalLine( "285px" ) );
                    } );
                    SEP.device.callbackOnDevice( "iphone" , function() {
                        if (type.toLowerCase() === "scrollmotion") {
                            marginLeft = "1px";
                            packageDetailOverlays = [ installUpdateButton , openExistingButton , removeButton ];
                        }
                        else {
                            paddingX = "-8px";
                            marginLeft = "-3px";
                            marginRight = "3px";
                            packageDetailOverlays = [ installUpdateButton , openExistingButton , shareButton , removeButton ];
                        }
                    } );
                    
                } else if( currentPackageState.state === "downloadavailable" || currentPackageState.state === "invalid" ) {
                    SEP.device.callbackOnDevice( "ipad" , function() {
                        openButtonTemp.relative = "parent";
                        openButtonTemp.horizontalAlign = "left";
                        openButtonTemp.verticalAlign = "top";
                        openButtonTemp.x = "0px";
                        openButtonTemp.y = "240px";
                        openButtonTemp.text = ipadspace + SEP.updateTranslationForString( "Open Existing" );
                        openButtonTemp.actions = [
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.closePackageModal();"
                                }
                            },
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.loadPackage( '" + type + "' , '" + localPath + "' , '" + title + "' , '"+ packageID + "' );"
                                }
                            }
                        ];

                        updateButtonTemp.relative = "parent";
                        updateButtonTemp.horizontalAlign = "left";
                        updateButtonTemp.verticalAlign = "top";
                        updateButtonTemp.x = "0px";
                        updateButtonTemp.y = "285px";
                        updateButtonTemp.text = ipadspace + SEP.updateTranslationForString( "Update Now" );
                        updateButtonTemp.actions = [
                            {
                                "action": "runScriptAction",
                                "trigger": "touchUpInside",
                                "target": "#jsBridge",
                                "data": {
                                    "script": "SEP.downloadPackage( '" + collectionViewId + "' );"
                                }
                            }
                        ];

                        SM.spawnInto( "collectionView_packageModal_details" , openButtonTemp );
                        SM.spawnInto( "collectionView_packageModal_details" , updateButtonTemp );
                        if( type.toLowerCase() === "scrollmotion" ) {
                            SM.spawnInto( "collectionView_packageModal_details" , { "overlayId": "action_btn_SEP_search_noEmail" } );
                        } else {
                            SM.spawnInto( "collectionView_packageModal_details" , { "overlayId": "action_btn_SEP_search" } );
                        }

                        SM.spawnInto( "collectionView_packageModal_details" , createHorizontalLine( "240px" ) );
                        SM.spawnInto( "collectionView_packageModal_details" , createHorizontalLine( "285px" ) );
                    } );
                    
                    SEP.device.callbackOnDevice( "iphone" , function() {
                        if (type.toLowerCase() === "scrollmotion") {
                            marginLeft = "1px";
                            packageDetailOverlays = [ updateAvailableButton , openExistingButton , removeButton ];
                        }
                        else {
                            paddingX = "-8x";
                            marginLeft = "-3px";
                            marginRight = "3px";
                            packageDetailOverlays = [ updateAvailableButton , openExistingButton , shareButton , removeButton ];
                        }
                    } );
                    
                } else {
                }

                // Animate here as oppose to on the overlay definition
                // This is due to an issue where overlays spawned into the container will spawn
                // At the width/height of the from value of the scale animation.
                SEP.device.callbackOnDevice( "ipad" , function() {
                    SM.runAction( {
                        "action": "animate",
                        "target": "collectionView_packageModal_details",
                        "trigger": "now",
                        "data": {
                            "animationId": "anim_scaleIn"
                        }
                    });
                } );
                SEP.device.callbackOnDevice( "iphone" , function() {
                    SM.spawnOnce( {
                        "overlayId": "collectionView_packageModal_details",
                        "overlays": [
                            {
                                "overlayId": "collectionView_packageModal_details-titleContainer",
                                "overlays": [
                                    {
                                        "overlayId": "collectionView_packageModal_details-title",
                                        "text": "<span style='font-family:AvenirLTStd-Heavy;'>" + title + "</span>"
                                    }
                                ]
                            },
                            {
                                "type":"container",
                                "relative": "parent",
                                "horizontalAlign":"center",
                                "verticalAlign":"center",
                                "width":"100%",
                                "height":"110px",
                                "layoutType": "flow",
                                "layoutOptions": {
                                    "paddingX": paddingX,
                                    "paddingY": "0px",
                                    "marginX": "0px",
                                    "marginY": "0px",
                                    "margin-top": "-2px",
                                    "margin-bottom": "0px",
                                    "margin-left": marginLeft,
                                    "margin-right": marginRight
                                },
                                "userScrolling": "horizontal",
                                "overlays": packageDetailOverlays
                            },
                            {
                                "overlayId": "collectionView_packageModal_details-info",
                                "text": infoString
                            },
                            {
                                "overlayId": "collectionView_packageModal_details-cancel"
                            }
                        ]
                    } );
                } );
                
            }
        } else { return; }
    }
    
    SEP.RegisterCollectionView.prototype.spawnPackageModal = packageSelected;
    SEP.RegisterCollectionView.prototype.spawnPackageModalLongPress = function() {
        var arr = Array.prototype.slice.call( arguments );
        arr.push( "longpress" );
        SEP.debug("arr is " + arr);
        packageSelected.apply( this , arr );
    };
    SEP.RegisterCollectionView.prototype.setcurrentPackageState = setcurrentPackageState;

    SEP.closePackageModal = closePackageModal;
    SEP.setcurrentPackageState = setcurrentPackageState;
    SEP.loadPackage = loadPackage;
    SEP.removePackage = removePackage;
    SEP.downloadPackage = downloadPackage;
    SEP.installUpdatePackage = updatePackage;
    SEP.emailPackage = emailPackage;

    SEP.network.deviceOffLineAction = deviceOffLineAction;
} ( SEP || {} ) );