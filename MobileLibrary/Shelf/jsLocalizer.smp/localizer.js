var Localizer = (function() {

	var stringsTable = {};
	
	var naiveCopy = function(object)
	{
		return JSON.parse(JSON.stringify(object));
	};

	var mergeObject = function(dest, src)
	{
		for(var propertyName in src) 
		{
			dest[propertyName] = naiveCopy(src[propertyName]);
		}	
	};
	
	var clearStringsTable = function(language, table)
	{
		stringsTable = {};	
	}

	var setStringsTable = function(language, table)
	{
		stringsTable[language] = {};
		stringsTable[language] = naiveCopy(table);
	};

	var localizedString = function(str) 
	{
		//not sure if caching this will be more efficient or not..
		var currentLang = SM.getProperty("#systemLanguageManager", "applicationLanguage");
		var localizedStr;
		
		if(stringsTable[currentLang] === undefined || stringsTable[currentLang][str] === undefined)
		{
			//always fall back on the passed key (likely in english)
			localizedStr = str;
		}
		else
		{
		    localizedStr = stringsTable[currentLang][str];
		}
		
		return localizedStr;
	};
	
	return {
localizedString: localizedString,
			setStringsTable: setStringsTable,
			clearStringsTable: clearStringsTable
                 };
}());
	
	

